# stack-chatbot



## Variables de configuración requeridas

- `ID` *Código de proyecto de bot que da BotTrainer*





## Variables de configuración opcionales
Todas estas variables son opcionales, se recomienda usar los valores por defecto salvo instalaciones especiales.
Revisar el archivo `docker-compose.yml` para ver los valores por defecto.
- `INSTANCE_PURPOSE` *Propósito de esta instancia, por ejemplo: 'Staging del proyecto X con funcionalidad experimental'*

- `STACK_CODE` *Código único dentro del nodo para crear puertos*


### Redes
- `COMMON_NET` *Red del common que le va a mandar los mensajes*
- `INGRESS_NET` *Red del manejador de tráfico entrante del nodo*
- `CMS_NET` *Red del CMS*

### Servidor de acciones
- `ACTIONS_IMAGE` *Imagen de Actions*
- `ACTIONS_API_URL` *IP y puerto de la api a donde apunta el server de acciones*

### Servidor de archivos
- `FILEBROWSER_IMAGE` *Imagen de Filebrowser*

### Servidor RASA
- `RASA_AUGMENTATION_FACTOR` *Numero de veces que se va a aumentar el dataset de entrenamiento*
- `RASA_IMAGE` *Imagen de Rasa*

